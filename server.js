const mongo = require("mongodb").MongoClient
const { ObjectId } = require("mongodb")
const createCsvWriter = require("csv-writer").createObjectCsvWriter
var url = "mongodb://localhost:7777/test"

const csvWriter = createCsvWriter({
    path: "./products.csv",
    header: [
        { id: "name", title: "NAME" },
        { id: "img", title: "IMAGE" },
        { id: "dimension", title: "DIMENSIONS" },
        { id: "cover_size", title: "COVER SIZE" },
    ],
})

let new_products = []

const open = async () => {
    return new Promise((resolve, reject) => {
        mongo.connect(
            url,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            },
            (err, client) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(client)
                }
            }
        )
    })
}

open()
    .then((response) => {
        return response.db("test")
    })
    .then((db) => {
        const products_col = db.collection("product")
        const fields_col = db.collection("field")
        const files_col = db.collection("file")
        const products_amount = products_col.countDocuments()
        let i = 0
        products_col.find().forEach((item) => {
            let new_product = {}
            let promiseAr = []
            let promiseVal = []
            for (const [key, value] of Object.entries(item.fields)) {
                promiseAr.push(fields_col.findOne({ _id: ObjectId(key) }))
                promiseVal[key] = value
            }
            Promise.all(promiseAr).then((fields) => {
                fields.forEach((field) => {
                    if (field !== null && "key" in field) {
                        switch (field.key) {
                            case "name":
                                new_product.name = promiseVal[field._id].en
                                break

                            case "image":
                                new_product.img = promiseVal[field._id]
                                break

                            case "dimension":
                                new_product.dimension = promiseVal[field._id]
                                break

                            case "cover_size":
                                new_product.cover_size = promiseVal[field._id]
                                break
                        }
                    }
                })
                if ("img" in new_product) {
                    files_col.findOne(
                        { _id: ObjectId(new_product.img.toString()) },
                        (err, item) => {
                            i++
                            new_product.img = item.url
                            new_products.push(new_product)
                            products_amount.then((count) => {
                                if (count == i) {
                                    csvWriter
                                        .writeRecords(new_products) 
                                        .then(() => {
                                            console.log("...Done")
                                        })
                                }
                            })
                        }
                    )
                }
            })
        })
    })
